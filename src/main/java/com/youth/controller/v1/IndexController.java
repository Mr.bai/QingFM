package com.youth.controller.v1;

import com.alibaba.fastjson.JSON;
import com.youth.commons.Constants;
import com.youth.pojo.*;
import com.youth.util.HttpClientTest;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by lenovo on 2018/5/31.
 */
@RestController(value = "v1Index")
@RequestMapping(value = "app/v1/radio/fm")
public class IndexController {

    private static final String HTTP_SERVER_URL = "http://rapi.qingting.fm/";
    private static final String LIVE_SERVER_URL = "http://lhttp.qingting.fm/";

    /**
     * 获取地域列表
     * @return
     */
    @GetMapping(value = "regions")
    public ResponseResult regions () {
        //http://rapi.qingting.fm/regions
        ResponseResult result = new ResponseResult();
        String url = "http://rapi.qingting.fm/regions";
        try{
            String body = HttpClientTest.sendGetData(url, "utf-8");
            RegionsResult regionsResult = JSON.parseObject(body, RegionsResult.class);
            if(regionsResult.getSuccess().equals("ok")){
                result = new ResponseResult(Constants.EXECUTESTATUS.成功, Constants.EXECUTEINFO.成功, Constants.EXECUTECODE.CODE_200, regionsResult.getData());
            }else {
                result = new ResponseResult(regionsResult.getSuccess(), Constants.EXECUTECODE.CODE_501);
            }
        }catch (Exception e) {
            e.printStackTrace();
            result = new ResponseResult("获取地域列表时发生异常", Constants.EXECUTECODE.CODE_500);
        }
        return result;
    }

    /**
     * 获取电台频道分类列表
     * @return
     */
    @GetMapping(value = "channel")
    public ResponseResult getChannel () {
        ResponseResult result = new ResponseResult();
        String url = "http://rapi.qingting.fm/categories";
        url += "?type=channel";
        try{
            String body = HttpClientTest.sendGetData(url, "utf-8");
            ChannelResult channelResult = JSON.parseObject(body, ChannelResult.class);
            if(channelResult.getSuccess().equals("ok")){
                result = new ResponseResult(Constants.EXECUTESTATUS.成功, Constants.EXECUTEINFO.成功, Constants.EXECUTECODE.CODE_200, channelResult.getData());
            }else {
                result = new ResponseResult(channelResult.getSuccess(), Constants.EXECUTECODE.CODE_501);
            }
        }catch (Exception e) {
            e.printStackTrace();
            result = new ResponseResult("获取电台频道分类列表时发生异常", Constants.EXECUTECODE.CODE_500);
        }
        return result;
    }

    /**
     * 获取电台网络频道列表
     * @return
     */
    @GetMapping(value = "program")
    public ResponseResult getProgram () {
        ResponseResult result = new ResponseResult();
        String url = "http://rapi.qingting.fm/categories";
        url += "?type=program";
        try{
            String body = HttpClientTest.sendGetData(url, "utf-8");
            ProgramResult programResult = JSON.parseObject(body, ProgramResult.class);
            if(programResult.getSuccess().equals("ok")){
                result = new ResponseResult(Constants.EXECUTESTATUS.成功, Constants.EXECUTEINFO.成功, Constants.EXECUTECODE.CODE_200, programResult.getData());
            }else {
                result = new ResponseResult(programResult.getSuccess(), Constants.EXECUTECODE.CODE_501);
            }
        }catch (Exception e) {
            e.printStackTrace();
            result = new ResponseResult("获取电台网络频道列表时发生异常", Constants.EXECUTECODE.CODE_500);
        }
        return result;
    }

    /**
     * 获取指定频道中电台列表
     * @param channel 频道类别ID
     * @param page 页数
     * @param pagesize 每页显示条数
     * @param showTotal 是否返回总条数
     * @return
     */
    @GetMapping(value = "channels")
    public ResponseResult getChannels (Integer channel,Integer page, Integer pagesize, Integer showTotal) {
        //http://rapi.qingting.fm/categories/259/channels?with_total=true&page=1&pagesize=12
        ResponseResult result = new ResponseResult();
        if(null == channel){
            return new ResponseResult("频道类别ID不允许为空", Constants.EXECUTECODE.CODE_400);
        }
        String url = HTTP_SERVER_URL + "categories/"+ channel + "/channels?";
        boolean isShowPageNum = false;
        if(null == showTotal || 0 != showTotal){// false 或者不传递，均不返回总条数
            isShowPageNum = true;
        }
        if(null == page)page = 1;
        if(null == pagesize)pagesize = 10;
        url += "with_total=" + isShowPageNum + "&page=" + page + "&pagesize=" + pagesize;
        try{
            String body = HttpClientTest.sendGetData(url, "utf-8");
            QingResult qingResult = new QingResult();
            if(isShowPageNum){
                qingResult = JSON.parseObject(body, ChannelsPageResult.class);
            }else {
                qingResult = JSON.parseObject(body, ChannelsResult.class);
            }

            if(qingResult.getSuccess().equals("ok")){
                result = new ResponseResult(Constants.EXECUTESTATUS.成功, Constants.EXECUTEINFO.成功, Constants.EXECUTECODE.CODE_200, qingResult.getData());
            }else {
                result = new ResponseResult(qingResult.getSuccess(), Constants.EXECUTECODE.CODE_501);
            }
        }catch (Exception e) {
            e.printStackTrace();
            result = new ResponseResult("获取指定频道中电台列表时发生异常", Constants.EXECUTECODE.CODE_500);
        }
        return result;
    }

    /**
     * 获取直播流地址
     * @param id
     * @return
     */
    @GetMapping(value = "live")
    public ResponseResult getLive (Integer id) {
        // http://lhttp.qingting.fm/live/{id}/64k.mp3
        ResponseResult result = new ResponseResult();
        if(null == id){
            return new ResponseResult("电台ID不允许为空", Constants.EXECUTECODE.CODE_400);
        }
        String url = LIVE_SERVER_URL + "live/"+ id + "/64k.mp3";
        return result = new ResponseResult(Constants.EXECUTESTATUS.成功, Constants.EXECUTEINFO.成功, Constants.EXECUTECODE.CODE_200, url);
    }











//    正在热播：
//    http://rapi.qingting.fm/recommendations/0/channel_list?more=true&replay=false
//
//    听回听：
//    http://rapi.qingting.fm/recommendations/0/replay_list?more=true&replay=true






    /**
     * 获取类别列表
     * @param type :
     *             1、channel
     *             2、program
     * @return
     */
    @Deprecated
    public ResponseResult getCategories (Integer type) {
        ResponseResult result = new ResponseResult();
        String url = "http://rapi.qingting.fm/categories";
        Object classObj = null;
        String typeProgram = "";
        if(1 == type){
            typeProgram = "channel";
            classObj = new ChannelResult();
        }else if(2 == type){
            typeProgram = "program";
            classObj = new ProgramResult();
        }

        if(StringUtils.isEmpty(typeProgram)){
            return new ResponseResult("type 不允许为空", Constants.EXECUTECODE.CODE_400);
        }

        url += "?type=" + typeProgram;

        HttpClientTest t = new HttpClientTest();
        try{
//            String body = HttpClientTest.sendGetData(url, "utf-8");
//            QingResult qingResult = JSON.parseObject(body, classObj.getClass());
//            if(regionsResult.getSuccess().equals("ok")){
//                result = new ResponseResult(Constants.EXECUTESTATUS.成功, Constants.EXECUTEINFO.成功, Constants.EXECUTECODE.CODE_200, regionsResult.getData());
//            }else {
//                result = new ResponseResult(regionsResult.getSuccess(), Constants.EXECUTECODE.CODE_501);
//            }
        }catch (Exception e) {
            e.printStackTrace();
            result = new ResponseResult("获取地域列表时发生异常", Constants.EXECUTECODE.CODE_500);
        }
        return result;

    }



}
