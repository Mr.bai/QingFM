package com.youth.controller.v1;

import com.alibaba.fastjson.JSON;
import com.youth.util.HttpClientTest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController(value = "v1Index")
@RequestMapping(value = "app/v1/one")
public class OneController {

    public static void main(String[] args) throws IOException {
        // left carousel-control
        String uri = "http://www.wufazhuce.com/";
//        HttpClientTest t = new HttpClientTest();
//        String body = t.sendGetData(uri, "utf-8");
//        int start = body.indexOf("class=\"carousel-inner\"");
//        int end = body.indexOf("<a class=\"left carousel-control\"");
//        String itmes = body.substring(start, end);
//
//
//        System.out.println(itmes.contains("<div class=\"item"));
//
//        if(itmes.contains("<div class=\"item")) {
//            int s1 = itmes.indexOf("<div class=\"item");
//            int e1 = itmes.indexOf("</div>\n" +
//                    "                    </div>");
//            System.out.println(itmes.substring(s1, e1));
//        }

        String hc = "class=\"carousel-inner\">\n" +
                "                                    <div class=\"item active\">\n" +
                "                        <a href=\"http://wufazhuce.com/one/4030\"><img class=\"fp-one-imagen\" src=\"http://image.wufazhuce.com/Fk7M0c6f7aN5obomp9a-RWJpGCYc\" alt=\"\" /></a>                        <div class=\"fp-one-imagen-footer\">\n" +
                "                            摄影                        </div>\n" +
                "                        <div class=\"fp-one-cita-wrapper\">\n" +
                "                            <div class=\"fp-one-titulo-pubdate\">\n" +
                "                                <p class=\"titulo\">VOL.3918</p>\n" +
                "                                <p class=\"dom\">29</p>\n" +
                "                                <p class=\"may\">Jun 2023</p>\n" +
                "                            </div>\n" +
                "                            <div class=\"fp-one-cita\">\n" +
                "                            <a href=\"http://wufazhuce.com/one/4030\">我想变成太阳，人们赞美我的热烈。不会注意我活了四十五亿年，大龄，孤身。不会催我找个月亮。</a>                            </div>\n" +
                "                            <div class=\"clearfix\"></div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                                    <div class=\"item\">\n" +
                "                        <a href=\"http://wufazhuce.com/one/4029\"><img class=\"fp-one-imagen\" src=\"http://image.wufazhuce.com/FoZKsSZX5xWNzV6U_tsIkGizI1tb\" alt=\"\" /></a>                        <div class=\"fp-one-imagen-footer\">\n" +
                "                            摄影                        </div>\n" +
                "                        <div class=\"fp-one-cita-wrapper\">\n" +
                "                            <div class=\"fp-one-titulo-pubdate\">\n" +
                "                                <p class=\"titulo\">VOL.3917</p>\n" +
                "                                <p class=\"dom\">28</p>\n" +
                "                                <p class=\"may\">Jun 2023</p>\n" +
                "                            </div>\n" +
                "                            <div class=\"fp-one-cita\">\n" +
                "                            <a href=\"http://wufazhuce.com/one/4029\">既然生活永远是这个样子，那么一个人过日子更好，因为那样就不会有另一个人来映射出你的悲伤。</a>                            </div>\n" +
                "                            <div class=\"clearfix\"></div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                                    <div class=\"item\">\n" +
                "                        <a href=\"http://wufazhuce.com/one/4028\"><img class=\"fp-one-imagen\" src=\"http://image.wufazhuce.com/FiknIPfYj8ccGaUXtrM4VsXViID_\" alt=\"\" /></a>                        <div class=\"fp-one-imagen-footer\">\n" +
                "                            摄影                        </div>\n" +
                "                        <div class=\"fp-one-cita-wrapper\">\n" +
                "                            <div class=\"fp-one-titulo-pubdate\">\n" +
                "                                <p class=\"titulo\">VOL.3916</p>\n" +
                "                                <p class=\"dom\">27</p>\n" +
                "                                <p class=\"may\">Jun 2023</p>\n" +
                "                            </div>\n" +
                "                            <div class=\"fp-one-cita\">\n" +
                "                            <a href=\"http://wufazhuce.com/one/4028\">厚厚的时间的灰尘和储存太久的眼泪是大人的味道。</a>                            </div>\n" +
                "                            <div class=\"clearfix\"></div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                                    <div class=\"item\">\n" +
                "                        <a href=\"http://wufazhuce.com/one/4027\"><img class=\"fp-one-imagen\" src=\"http://image.wufazhuce.com/FjLCn4xuXNNyWf2b_EuYz7UGUW5i\" alt=\"\" /></a>                        <div class=\"fp-one-imagen-footer\">\n" +
                "                            摄影                        </div>\n" +
                "                        <div class=\"fp-one-cita-wrapper\">\n" +
                "                            <div class=\"fp-one-titulo-pubdate\">\n" +
                "                                <p class=\"titulo\">VOL.3915</p>\n" +
                "                                <p class=\"dom\">26</p>\n" +
                "                                <p class=\"may\">Jun 2023</p>\n" +
                "                            </div>\n" +
                "                            <div class=\"fp-one-cita\">\n" +
                "                            <a href=\"http://wufazhuce.com/one/4027\">有一种治疗失意的疗法叫“耗尽”，只要我们反复诉说同一件事，就能消耗掉足够多的痛苦与激情。<br />\n" +
                "</a>                            </div>\n" +
                "                            <div class=\"clearfix\"></div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                                    <div class=\"item\">\n" +
                "                        <a href=\"http://wufazhuce.com/one/4026\"><img class=\"fp-one-imagen\" src=\"http://image.wufazhuce.com/FiXBCpYU0SdeIMHKGMO3pkDX_wsD\" alt=\"\" /></a>                        <div class=\"fp-one-imagen-footer\">\n" +
                "                            摄影                        </div>\n" +
                "                        <div class=\"fp-one-cita-wrapper\">\n" +
                "                            <div class=\"fp-one-titulo-pubdate\">\n" +
                "                                <p class=\"titulo\">VOL.3914</p>\n" +
                "                                <p class=\"dom\">25</p>\n" +
                "                                <p class=\"may\">Jun 2023</p>\n" +
                "                            </div>\n" +
                "                            <div class=\"fp-one-cita\">\n" +
                "                            <a href=\"http://wufazhuce.com/one/4026\">年轻的时候，我也会为了引人注目而周旋、逞强。上了年纪以后，却开始讨厌被他人期待了。太麻烦了，我希望不要有人对我抱有期待。</a>                            </div>\n" +
                "                            <div class=\"clearfix\"></div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                                    <div class=\"item\">\n" +
                "                        <a href=\"http://wufazhuce.com/one/4025\"><img class=\"fp-one-imagen\" src=\"http://image.wufazhuce.com/Fvyx2zkhPZdfEVuWm2shns_Zu-jK\" alt=\"\" /></a>                        <div class=\"fp-one-imagen-footer\">\n" +
                "                            摄影                        </div>\n" +
                "                        <div class=\"fp-one-cita-wrapper\">\n" +
                "                            <div class=\"fp-one-titulo-pubdate\">\n" +
                "                                <p class=\"titulo\">VOL.3913</p>\n" +
                "                                <p class=\"dom\">24</p>\n" +
                "                                <p class=\"may\">Jun 2023</p>\n" +
                "                            </div>\n" +
                "                            <div class=\"fp-one-cita\">\n" +
                "                            <a href=\"http://wufazhuce.com/one/4025\">我是一个怎样的人，很大程度上是由我所处的环境，而不是由我的所谓本性决定的。</a>                            </div>\n" +
                "                            <div class=\"clearfix\"></div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                                    <div class=\"item\">\n" +
                "                        <a href=\"http://wufazhuce.com/one/4023\"><img class=\"fp-one-imagen\" src=\"http://image.wufazhuce.com/Flbv6kRUjzYuHFP5dXVcRhBHzjjC\" alt=\"\" /></a>                        <div class=\"fp-one-imagen-footer\">\n" +
                "                            摄影                        </div>\n" +
                "                        <div class=\"fp-one-cita-wrapper\">\n" +
                "                            <div class=\"fp-one-titulo-pubdate\">\n" +
                "                                <p class=\"titulo\">VOL.3912</p>\n" +
                "                                <p class=\"dom\">23</p>\n" +
                "                                <p class=\"may\">Jun 2023</p>\n" +
                "                            </div>\n" +
                "                            <div class=\"fp-one-cita\">\n" +
                "                            <a href=\"http://wufazhuce.com/one/4023\">健康人际关系的基本要素就是坦诚——愿意摘下面具。这意味着愿意透露我们的想法、感受和经历，既不自我设限，也不装腔作势；意味着不摆架子，不假装高兴，不夸大成绩，也不试图满足所有人的需求。</a>                            </div>\n" +
                "                            <div class=\"clearfix\"></div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                                </div>";
        ;


//        System.out.println(hc.trim());


//

//        replace(" ", “”)
        hc = hc.replace(" ", "");
//        hc = hc.replace("> ", ">");
//        System.out.println(hc);


        hc = hc.replace("\n", "");
//        System.out.println(hc);

        List<String> itemContents = new ArrayList<>();

        while (hc.contains("<divclass=\"item")) {
            int s1 = hc.indexOf("<divclass=\"item");
            int e1 = hc.indexOf("<divclass=\"clearfix\"></div></div></div>");
            String nhc = hc.substring(s1, e1);
            itemContents.add(nhc);
            int s2 = hc.indexOf(nhc) + nhc.length() + "<divclass=\"clearfix\"></div></div></div>".length();
            hc = hc.substring(s2, hc.length());
        }
//        System.out.println(itemContents);
        itemContents.forEach((String e) -> {
            System.out.println(e);
            System.out.println(getLink(e));
            System.out.println(getImage(e));
            System.out.println(getOneContent(e));
            Map<String, String> res = getDateInfo(e);
            System.out.println();
        });

//        System.out.println("响应结果：" + itmes);
    }

    public static String getLink(String args) {
        int s1 = args.indexOf("<ahref=\""),
        e1 = args.indexOf("\">", s1);
        String content = args.substring(s1, e1);
        content = content.replace("<ahref=\"", "");
        //String content = args.substring(args.indexOf("http://wufazhuce.com/one/"), args.indexOf("\"><imgclass"));
        return content;
    }

    public static String getImage(String args) {
        int s1 = args.indexOf("src=\""),
                e1 = args.indexOf("\"alt=\"\"/>");
        String content = args.substring(s1, e1);
        content = content.replace("src=\"", "");
//        String content = args.substring(args.indexOf("http://image.wufazhuce.com/"), args.indexOf("\"alt=\"\"/>"));
        return content;
    }

    public static String getOneContent(String args) {

        String link = getLink(args);

        int s1 = args.indexOf("<divclass=\"fp-one-cita\"><ahref=\""+link+"\">"),
                e1 = args.indexOf("</a></div>");
        String content = args.substring(s1, e1);
        content = content.replace("<divclass=\"fp-one-cita\"><ahref=\""+link+"\">", "");
//        String content = args.substring(args.indexOf("http://image.wufazhuce.com/"), args.indexOf("\"alt=\"\"/>"));
        return content;
    }

    public static String getDateInfo(String args) {
        Map<String, String> re = new HashMap<>();

        int s1 = args.indexOf("<pclass=\"titulo\">"),
                e1 = args.indexOf("</p>", s1);
        String titulo = args.substring(s1, e1);
        titulo = titulo.replace("<pclass=\"titulo\">", "");

        int s2 = args.indexOf("<pclass=\"dom\">"),
                e2 = args.indexOf("</p>", s2);
        String dom = args.substring(s2, e2);
        dom = dom.replace("<pclass=\"dom\">", "");

        int s3 = args.indexOf("<pclass=\"may\">"),
                e3 = args.indexOf("</p>", s3);
        String may = args.substring(s3, e3);
        may = may.replace("<pclass=\"may\">", "");

        re.put("titulo", titulo);
        re.put("dom", dom);
        re.put("may", may);
        return JSON.toJSONString(re);
    }

}
