package com.youth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class FmshowApplication {

	public static void main(String[] args) {
		SpringApplication.run(FmshowApplication.class, args);
	}

}
