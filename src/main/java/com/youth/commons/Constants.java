package com.youth.commons;

import java.text.MessageFormat;

/**
 * Created by lenovo on 2018/5/31.
 */
public class Constants {


    /**
     * Shiro缓存实现方式
     */
    public static final String CACHE_MODEL_REDIS = "redis";
    public static final String CACHE_MODEL_EHCACHE = "ehcache";

    /**
     * 导出最大记录数（不要超过Excel的最大记录：256*256=65536）
     */
    public static final int MAX_REPORT_ROWS = 50000;

    public static final class EXECUTEINFO{
        public static final String 成功 = "SUCCESS";
        public static final String 失败 = "FAIL";
        public static final String 异常 = "ERROR";

        public static final String getInfo(String infoType, String info){
            String template = "{0}:{1}";
            return MessageFormat.format(template, infoType, info);
        }
    }

    /**
     * @Title Constants.java
     * @Project BAPI
     * @Package com.hopechart.bapi.commons.constant
     * @Description TODO 故障码
     * 			故障码规则：
     * 			3+接口序号+ code码
     * 			接口序号不够2位，则用0补齐；
     * 			code码规则：三位数，从100开始；
     * 							1XX 为限制类错误
     * 							2XX 为调用方错误
     * 							3XX 为平台错误
     * 							4XX 为调用误码
     * 			如接口序号2，code码位1，则该故障码为302101
     * @See
     * @Author BaiKeyang@hopechart.com
     * @ComputerUser lenovo
     * @Time 2016年11月30日 上午10:50:23
     * @Version v1.0.0
     * @Copyright  2016  <a href="http://www.Hopechart.com/" target="_blank">Hopechart Soft. </a>
     */
    public static final class EXECUTECODE {
        public static final int CODE_100 = 100;
        public static final int CODE_101 = 101;
        public static final int CODE_102 = 102;

        public static final int CODE_200 = 200;
        public static final int CODE_400 = 400;
        public static final int CODE_404 = 404;
        public static final int CODE_500 = 500;
        public static final int CODE_501 = 501;
    }

    public static final class EXECUTESTATUS{
        public static final boolean 成功 = true;
        public static final boolean 失败 = false;
    }

}
