package com.youth.pojo;

import java.util.List;

/**
 * Created by lenovo on 2018/5/31.
 */
public class Categories {

    List<Channel> channel_categories;// 分类频道

    List<Program> program_categories;// 网络频道

    List<Regions> regions;// 国家区域：国家台、省市台、网络台……

    List<Regions> regions_map;// 全国区域

    public List<Channel> getChannel_categories() {
        return channel_categories;
    }

    public void setChannel_categories(List<Channel> channel_categories) {
        this.channel_categories = channel_categories;
    }

    public List<Program> getProgram_categories() {
        return program_categories;
    }

    public void setProgram_categories(List<Program> program_categories) {
        this.program_categories = program_categories;
    }

    public List<Regions> getRegions() {
        return regions;
    }

    public void setRegions(List<Regions> regions) {
        this.regions = regions;
    }

    public List<Regions> getRegions_map() {
        return regions_map;
    }

    public void setRegions_map(List<Regions> regions_map) {
        this.regions_map = regions_map;
    }
}
