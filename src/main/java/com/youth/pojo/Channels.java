package com.youth.pojo;

/**
 * 频道列表
 * Created by lenovo on 2018/5/31.
 */
public class Channels {

    int content_id;

    String content_type;

    String cover;

    String title;

    String description;

    Nowplay nowplaying;

    int audience_count;

    String liveshow_id;

    public int getContent_id() {
        return content_id;
    }

    public void setContent_id(int content_id) {
        this.content_id = content_id;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Nowplay getNowplaying() {
        return nowplaying;
    }

    public void setNowplaying(Nowplay nowplaying) {
        this.nowplaying = nowplaying;
    }

    public int getAudience_count() {
        return audience_count;
    }

    public void setAudience_count(int audience_count) {
        this.audience_count = audience_count;
    }

    public String getLiveshow_id() {
        return liveshow_id;
    }

    public void setLiveshow_id(String liveshow_id) {
        this.liveshow_id = liveshow_id;
    }
}
