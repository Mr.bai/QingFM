package com.youth.pojo;

import com.youth.commons.Constants;

import java.io.Serializable;

/**
 * Created by lenovo on 2018/5/31.
 */
public class ResponseResult implements Serializable {


    private static final long serialVersionUID = -1209067831575724458L;

    /**
     * 成功标记 true：成功；false：失败
     */
    private boolean status = Constants.EXECUTESTATUS.失败;

    /**
     * 消息
     */
    private String info = Constants.EXECUTEINFO.失败;

    /**
     * 数据
     */
    private Object data;

    /**
     * 信息码
     */
    private Integer code = Constants.EXECUTECODE.CODE_404;

    public ResponseResult (){}

    public ResponseResult(String info, int code){
        this.info = info;
        this.code = code;
    }

    public ResponseResult(boolean status, String info, int code){
        this.status = status;
        this.info = info;
        this.code = code;
    }

    public ResponseResult(boolean status, String info, int code, Object data){
        this.status = status;
        this.info = info;
        this.code = code;
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
