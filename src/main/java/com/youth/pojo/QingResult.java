package com.youth.pojo;

import java.util.List;

/**
 * Created by lenovo on 2018/5/31.
 */
public class QingResult<E> {

    String Success;

    E Data;

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }

    public E getData() {
        return Data;
    }

    public void setData(E data) {
        Data = data;
    }
}
