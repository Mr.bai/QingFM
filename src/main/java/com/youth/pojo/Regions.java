package com.youth.pojo;

/**
 * Created by lenovo on 2018/5/31.
 */
public class Regions {

    int id;

    String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
