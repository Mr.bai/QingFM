package com.youth.pojo;

import java.io.Serializable;

/**
 * Created by lenovo on 2018/5/31.
 */
public class Program implements Serializable {

    int id;

    String title;

    String cover;

    String[] channel_titles;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String[] getChannel_titles() {
        return channel_titles;
    }

    public void setChannel_titles(String[] channel_titles) {
        this.channel_titles = channel_titles;
    }
}
