package com.youth.pojo;

/**
 * Created by lenovo on 2018/5/31.
 */
public class Nowplay {

    int id;

    String title;

    String[] broadcasters;

    String start_time;

    int duration;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getBroadcasters() {
        return broadcasters;
    }

    public void setBroadcasters(String[] broadcasters) {
        this.broadcasters = broadcasters;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
