package com.youth.pojo;

import java.util.List;

/**
 * Created by lenovo on 2018/6/1.
 */
public class ChannelsPage {

    List<Channels> items;

    int total;

    public List<Channels> getItems() {
        return items;
    }

    public void setItems(List<Channels> items) {
        this.items = items;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
