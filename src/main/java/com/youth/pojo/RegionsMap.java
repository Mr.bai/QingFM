package com.youth.pojo;

import java.io.Serializable;

/**
 * Created by lenovo on 2018/5/31.
 */
public class RegionsMap implements Serializable {

    int id;

    String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
