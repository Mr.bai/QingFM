#!/bin/sh
echo "开始启动"
# 关闭程序
# fileName为jar包的名称
fileName=fmshow-0.1.jar
pid=$(ps -ef | grep $fileName| grep -v "grep" | awk '{print $2}')
kill -9 $pid
# 启动项目
nohup java -jar fmshow-0.1.jar > fmshow-0.1.log &
echo "启动完成"